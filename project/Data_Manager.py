
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm as cm
import matplotlib.cm as cm

import scipy
import scipy.cluster.hierarchy as sch
import time

from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn import cluster, datasets
from sklearn.neighbors import kneighbors_graph
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import silhouette_samples, silhouette_score, calinski_harabaz_score

class DataManager:
	def __init__(self,df):
		self.__df=df
		self.__rets=None

	def get_df(self):
		return self.__df

	def getRetsList(self,lista):
		return self.__rets[lista]

	def getRets(self):
		return self.__rets

	def DataToRetLog(self):
		self.__rets = np.log(self.__df) - np.log(self.__df.shift(1))
		self.__rets = self.__rets.drop(self.__rets.index[[0]])

	def ComputeMatrix(self):
		corr=self.__rets.corr()
		cov=self.__rets.cov()
		Dis=self.__correlDist(corr).as_matrix()
		disD=self.__disInterCol(corr.shape[0],Dis)

		self.D0=corr
		self.D1=Dis
		self.D2=disD
		self.D3=cov

	def Compute(self):
		self.DataToRetLog()
		self.ComputeMatrix()

	def __correlDist(self,corr):
		dist = ((1 - corr) / 2.)**.5
		return dist

	def __disInterCol(self,mov,Dis):
		disD=np.zeros((mov,mov))
		for i in range(mov):
			for j in range(mov):
				disD[i,j]=np.sqrt(np.sum((Dis[:,i]-Dis[:,j])**2.))
		return disD

	def plot_matrix(self,title):
		fig = plt.figure()
		ax1 = fig.add_subplot(111)
		cmap = cm.get_cmap('jet', 30)
		cax = ax1.matshow(self.D0, interpolation="nearest", cmap=cmap, vmin=0, vmax=1)

		plt.grid(False)
		plt.title(title)

		plt.xticks([])
		plt.yticks([])
		cbar = fig.colorbar(cax, ticks=[-1,0,1])

		plt.show()
