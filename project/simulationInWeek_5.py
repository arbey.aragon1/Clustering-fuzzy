import pandas as pd
import numpy as np
from utils import getKey
from OptSharpe import OptSharpe
from dataByPlot import StorageSimulation

def optByPortfolio(groupsObj, dataByTrain):
    data = {}
    for key, value in groupsObj.items():
        data[key] = {}
        data[key]['listNames']=value['listNames']
        data[key]['dataByTrain']=dataByTrain[value['listNames']]
        optimizador = OptSharpe(returns = dataByTrain[value['listNames']])
        optimizador.Compute()
        data[key]['retorno'],data[key]['riesgo'],c= optimizador.Sharp()
        data[key]['pesos'] = optimizador.getW()
        data[key]['mediaRetornos'] = optimizador.getReturnsMean()
        
    sumRiskInverse = 0.0
    for key, value in data.items():
        val = float(1.0)/float(value['riesgo'])
        sumRiskInverse += val
        data[key]['pesoPortafolio'] = val
    
    for key, value in data.items():
        data[key]['pesoPortafolio'] = float(data[key]['pesoPortafolio']) / float(sumRiskInverse)
        data[key]['pesosNPR'] = data[key]['pesos'] * data[key]['pesoPortafolio']
    return data

def retByPortfolio(groupsObj, dataOfGroups, dataByPredict, investment):
    vec_ret = np.array([])
    vec_w = np.array([])
    for key, value in groupsObj.items():
        dbp = dataByPredict[value['listNames']]
        dbp_matrix = dbp.as_matrix()
        vec_retornos_puros = ((dbp_matrix[-1]-dbp_matrix[0])/dbp_matrix[0]+1.0)
        vec_pesos_x_grupo = dataOfGroups[key]['pesosNPR']
        vec_ret = np.concatenate((vec_ret, vec_retornos_puros), axis=0)
        vec_w = np.concatenate((vec_w, vec_pesos_x_grupo), axis=0)
        
    ret_portafolio = investment * np.dot(vec_w, vec_ret)
    return ret_portafolio


def simulInWeek(s, keyDataPlot):
    investment = s['capital']
    
    storage=StorageSimulation.getInstance({})
    rdataByTrain = s['dataByClustering_returns_lastInterval']
    
    dataClustersToProcessing = []
    for (rdateI,rdataI),(dateI,dataI) in zip(s['dataByTest_returns'].groupby(pd.TimeGrouper(s['frec'])), 
                                             s['dataByTest'].groupby(pd.TimeGrouper(s['frec']))):
        if(rdataI.shape[0]>0):
            dataByTest = dataI   
            dataOfGroupsByInterval = optByPortfolio(s['groupsObj'].copy(), rdataByTrain.copy())
            dataClustersToProcessing.append({
                'dateInitTest': dateI,
                'rdateInitTest': rdateI,
                'dateHead2Train': rdataByTrain.copy().head(1).index,
                'dateTail2Train': rdataByTrain.copy().tail(1).index,
                'data': dataOfGroupsByInterval.copy()
            })
            investment = retByPortfolio(s['groupsObj'].copy(), 
                                        dataOfGroupsByInterval.copy(), 
                                        dataByTest.copy(), 
                                        investment)
            
            storage.SAVE_SECOND(keyDataPlot,'listData',[dateI,investment])
            dataByTrain = rdataI
    storage.SAVE_CLUSTER_DATA(keyDataPlot, dataClustersToProcessing)
    s['capital'] = investment
    return s