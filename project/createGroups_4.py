def getGrupsName(names, y_pred, index):
    lista = []
    for n,y in zip(names,y_pred):
        if(y==index):
            lista.append(n)
    return lista

def createGroupsObjs(s):
    namesAct = s['dataByTest'].columns.values
    
    s['groupsObj'] = {}
    item = 0
    for i in range(max(s['y_pred'])):
        namesOfGroup = getGrupsName(namesAct, s['y_pred'], i)
        if(len(namesOfGroup) > 0):
            s['groupsObj']['group-'+str(item)] = {'listNames': namesOfGroup}
            item += 1
    return s    
