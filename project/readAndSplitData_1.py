import pandas as pd
import numpy as np
import scipy
import time
from datetime import datetime, timedelta
from utils import getKey

folOut = 'dataOutSim'
clusFuzz = 4
indeximg = 0


def streamHistData(s):
    def mainStream():#observer
        ############### Carga datos
        df = pd.read_csv('data/Datos.csv',delimiter=';',index_col='Timestamp',decimal='.')
        df.index = pd.to_datetime(df.index)
        
        ############### Calculo de retornos
        rets = np.log(df) - np.log(df.shift(1))
        rf = rets.drop(rets.index[[0]])
        df = df.drop(df.index[[0]])
        
        ############### Primeras 3 semanas
        dfWeeks = df.groupby(pd.Grouper(freq='W'))
        rfWeeks = rf.groupby(pd.Grouper(freq='W'))
        
        for (dateW,groupW),(rdateW,rgroupW) in zip(dfWeeks, rfWeeks):
            yield([groupW, rgroupW])
            
            
    def dataSplit(dat, buff):
        
        dat['dataByClustering'] = pd.concat([buff[0][0], buff[1][0], buff[2][0]])
        dat['dataByTest'] = buff[3][0]
        
        dat['dataByClustering_returns'] = pd.concat([buff[0][1], buff[1][1], buff[2][1]])
        dat['dataByTest_returns'] = buff[3][1]
        
        
        for dateI,retsI in dat['dataByClustering_returns'].groupby(pd.TimeGrouper(dat['frec'])):
            lastInterval = retsI
        dat['dataByClustering_returns_lastInterval'] = lastInterval
        
        return dat.copy()

    buf = 4
    stm = mainStream()
    lista = []
    for i,it in enumerate(stm):
        lista.append(it)
        if(i>=buf-1):
            yield(dataSplit(s, lista))
            del lista[0]
