
import pandas as pd
import numpy as np
import scipy
import scipy.cluster.hierarchy as sch
import time

from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn import cluster, datasets
from sklearn.neighbors import kneighbors_graph
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import silhouette_samples, silhouette_score, calinski_harabaz_score
import skfuzzy as fuzz
import os

from datetime import datetime, timedelta
from utils import getKey


def fitt(D, C, algoritm, clusLib = 'sklearn'):
    if(clusLib == 'sklearn'):
        algoritm.n_clusters = C
        algoritm.fit(D)
        return algoritm.labels_.astype(np.int), None
    else:
        cntr, u, u0, d, jm, p, fpc = fuzz.cluster.cmeans(D, C, 2, error=0.00005, maxiter=10000, init=None)
        return np.argmax(u, axis=0), fpc
    
def getMatrixCorr(y_pred, corr):
    idx1=[i[1] for i in sorted([[j,i] for i,j in enumerate(y_pred)], key=getKey)]
    Dtem=corr.as_matrix()
    Dtem = Dtem[idx1,:]
    Dtem = Dtem[:,idx1]
    return Dtem


############### Algoritmos de clustering
def fitPredict_AL(s, ncluster = 6):
    s['nameAl'] = name = 'AL'
    s['ncluster'] = ncluster
    s['metric'] = metric = 'precomputed'
    s['clusLib'] = 'sklearn'
    D = s['dmbc']

    algoritm = cluster.AgglomerativeClustering(linkage = "average", affinity = metric)
    s['y_pred'] = fitt(D, ncluster, algoritm)[0]
    s['matrix_pred'] = getMatrixCorr(s['y_pred'], s['corr'])
    return s

def fitPredict_CL(s, ncluster = 10):
    s['nameAl'] = name = 'CL'
    s['ncluster'] = ncluster
    s['metric'] = metric = 'precomputed'
    s['clusLib'] = 'sklearn'
    D = s['dmbc']

    algoritm = cluster.AgglomerativeClustering(linkage = "complete", affinity = metric)
    s['y_pred'] = fitt(D, ncluster, algoritm)[0]
    s['matrix_pred'] = getMatrixCorr(s['y_pred'], s['corr'])
    return s

def fitPredict_WM(s, ncluster = 10):
    s['nameAl'] = name = 'WM'
    s['ncluster'] = ncluster
    s['metric'] = metric = 'precomputed'
    s['clusLib'] = 'sklearn'
    D = s['dmbc']

    connectivity = kneighbors_graph(D, n_neighbors=10, include_self=False)
    connectivity = 0.5 * (connectivity + connectivity.T)
    algoritm = cluster.AgglomerativeClustering(linkage = 'ward', connectivity = connectivity)
    s['y_pred'] = fitt(D, ncluster, algoritm)[0]
    s['matrix_pred'] = getMatrixCorr(s['y_pred'], s['corr'])
    return s

def fitPredict_Fuzzy(s, ncluster = 6):
    s['nameAl'] = name = 'FUZZY'
    s['ncluster'] = ncluster
    s['metric'] = metric = 'precomputed'
    s['clusLib'] = 'fuzzy'
    D = s['dmbc']
    
    algoritm = None
    s['y_pred'] = fitt(D, ncluster, algoritm, 'fuzzy')[0]
    s['matrix_pred'] = getMatrixCorr(s['y_pred'], s['corr'])
    return s

###### funcion para exportar
def getNameAl(var):
    if(var == 1):
        return 'AL'
    elif(var == 2):
        return 'CL'
    elif(var == 3):
        return 'WM'
    elif(var == 4):
        return 'FUZZY'

###### funcion para exportar
def fitPredict(s):
    var = s['index']
    if(var == 1):
        return fitPredict_AL(s)
    
    elif(var == 2):
        return fitPredict_CL(s)
    
    elif(var == 3):
        return fitPredict_WM(s)
    
    elif(var == 4):
        return fitPredict_Fuzzy(s)