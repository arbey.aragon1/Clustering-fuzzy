class StorageSimulation():
    __config = None
    __instance = None
    __data = None
    __dataClusters = None
                
    def __init__(self, config):
        if StorageSimulation.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            StorageSimulation.__instance = self
            StorageSimulation.__config = config
            StorageSimulation.__data = {}
            StorageSimulation.__dataClusters = {}

    @staticmethod
    def getInstance(config):
        if StorageSimulation.__instance == None:
            StorageSimulation(config)
        return StorageSimulation.__instance 
    
    def SET_DATA(self, data):
        StorageSimulation.__data = data

    def GET_DATA(self):
        return StorageSimulation.__data

    def SAVE_FIRST(self, key, data):
        StorageSimulation.__data[key] = data

    def SAVE_SECOND(self, key1, key2, data):
        StorageSimulation.__data[key1][key2].append(data)

    def SAVE_CLUSTER_DATA(self, key, data):
        StorageSimulation.__dataClusters[key] = data

    def GET_CLUSTER_DATA(self):
        return StorageSimulation.__dataClusters
