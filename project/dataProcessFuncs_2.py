
import pandas as pd
import numpy as np
from utils import getKey

############### Funcion de Cambio de matris de correlacion a distancia
def correlDist(corr):
    dist = ((1 - corr) / 2.)**.5
    return dist

def disInterCol(mov,Dis):
    disD=np.zeros((mov,mov))
    for i in range(mov):
        for j in range(mov):
            disD[i,j]=np.sqrt(np.sum((Dis[:,i]-Dis[:,j])**2.))
    return disD

def calcDataInfo(s):
    ############### Calculo de retornos
    rets = s['dataByClustering_returns']
    
    ############## Calculo de matrices
    s['corr'] = rets.corr()
    s['cov'] = rets.cov()
    ## correlation matrix to distance matrix
    s['cmtdm'] = correlDist(s['corr']).as_matrix()
    ## distance matrix between columns
    s['dmbc'] = disInterCol(s['corr'].shape[0], s['cmtdm'])
    
    ############## ultimo intervalo de tiempo de los retornos
    return s