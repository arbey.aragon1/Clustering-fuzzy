import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm as cm
import matplotlib.cm as cm

import scipy
import scipy.cluster.hierarchy as sch
import time

from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn import cluster, datasets
from sklearn.neighbors import kneighbors_graph
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import silhouette_samples, silhouette_score, calinski_harabaz_score
import Data_Manager as MgClus
import math
import scipy as sp
import scipy.optimize as scopt

from datetime import datetime, timedelta


class OptSharpe:
	def __init__(self,returns=None,W=None):
		self.__returns=returns
		self.__rlr=0.35/100.
		self.__X=W

		self.__maxLim=1.0
		self.__minLim=0#(1./100.)

		self.__setMin=(1./100.)

	def CompMedCov(self):
		self.cov=self.__returns.cov().as_matrix()
		self.med=self.__returns.mean().as_matrix()
	    

	def Compute(self):
		self.CompMedCov()
		def Xportafolio(X,med,cov,rlr):
			riesgo=np.sqrt(np.dot(np.dot(X,cov),X.T))
			retorno=np.sum(X*med)
			return -(retorno-rlr)/riesgo
	    
		W=np.ones(self.__returns.shape[1])
		b_ = [(self.__minLim,self.__maxLim) for i in range(self.__returns.shape[1])]
		c_ = ({'type':'eq', 'fun': lambda W: sum(W)-1. })
		result = scopt.minimize(fun=Xportafolio,x0=W, args=(self.med,self.cov,self.__rlr),constraints=c_, bounds=b_,method='SLSQP')
		
		q=result.x
		if(float(q[np.where( q >= self.__setMin )].shape[0]) > 0):
			tem=q[np.where( q < self.__setMin )]
			q[q < self.__setMin ]=0
			q[q >= self.__setMin ]+=float(np.sum(tem))/float(q[np.where( q >= self.__setMin )].shape[0])

		#if math.isnan(result.x[0]) or True:
		#	print('++++++++++++++++++++++')
		#	print(self.__returns)
		#	print('++++++++++')
		self.__X=q


	def Sharp(self):
		riesgo=np.sqrt(np.dot(np.dot(self.__X,self.cov),self.__X.T))
		retorno=np.sum(self.__X*self.med)
		return retorno,riesgo,(retorno-self.__rlr)/riesgo


	def getW(self):
		return self.__X

	def setW(self,x):
		self.__X = x

	def getReturns(self):
		return self.__returns

	def getReturnsMean(self):
		return self.med

	def setReturns(self,returns):
		self.__returns = returns


