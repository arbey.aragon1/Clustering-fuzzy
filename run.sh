sudo docker kill $(sudo docker ps -a -q)
sudo docker rm $(sudo docker ps -a -q)
sudo docker build -t clus:1 .
sudo docker run -it \
    --name clustering \
    -p 8888:8888 -d \
    -v $(pwd)/project:/home/jovyan/project \
    clus:1 bash

sudo docker exec -it clustering bash
